/**
 * @author akovalenko
 * @date 19.02.2015
 */
package demo.enumParser.classes
{
import util.EnumUtil;

public class DemoEnumStrings
{
  public static const STRING_CONSTANT1:String = "String Constant 1";
  public static const STRING_CONSTANT2:String = "String Constant 2";
  public static const STRING_CONSTANT3:String = "String Constant 3";
  public static const STRING_CONSTANT4:String = "String Constant 4";
  public static const STRING_CONSTANT5:String = "String Constant 5";

  private static var _ALL_CONSTANTS:Array;

  public static function get ALL_CONSTANTS():Array
  {
    if( !_ALL_CONSTANTS )
    {
      _ALL_CONSTANTS = EnumUtil.getClassConstantsArray( DemoEnumStrings );
    }

    return _ALL_CONSTANTS;
  }
}
}
