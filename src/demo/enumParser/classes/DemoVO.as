/**
 * @author akovalenko
 * @date 19.02.2015
 */
package demo.enumParser.classes
{
public class DemoVO
{
  public var id:int;

  public var name:String;

  public function DemoVO( id:int, name:String )
  {
    this.id = id;
    this.name = name;
  }

  public function toString():String
  {
    return id + ": " + name;
  }
}
}
