/**
 * @author akovalenko
 * @date 19.02.2015
 */
package demo.enumParser.classes
{
import util.EnumUtil;

public class DemoEnumClasses
{
  public static const CLASS_CONSTANT1:DemoVO = new DemoVO( 1, "Name 1" );
  public static const CLASS_CONSTANT2:DemoVO = new DemoVO( 2, "Name 2" );
  public static const CLASS_CONSTANT3:DemoVO = new DemoVO( 3, "Name 3" );
  public static const CLASS_CONSTANT4:DemoVO = new DemoVO( 4, "Name 4" );
  public static const CLASS_CONSTANT5:DemoVO = new DemoVO( 5, "Name 5" );

  private static var _ALL_CONSTANTS:Vector.<DemoVO>;

  public static function get ALL_CONSTANTS():Vector.<DemoVO>
  {
    if( !_ALL_CONSTANTS )
    {
      _ALL_CONSTANTS = Vector.<DemoVO>( EnumUtil.getClassTypedConstants( DemoEnumClasses, DemoVO, new Vector.<DemoVO> ) );
    }

    return _ALL_CONSTANTS;
  }
}
}
