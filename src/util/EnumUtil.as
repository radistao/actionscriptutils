/**
 * @author akovalenko
 * @date 19.02.2015
 */
package util
{
import flash.utils.getQualifiedClassName;

import mx.utils.DescribeTypeCache;

/**
 * These utils are intended to use some classes as constant enums and list all static constants of a class.
 */
public class EnumUtil
{
  /**
   * Parse <code>clazz</code> for <b><code>public static const</code></b> values and push all of them
   * into <code>vectorOrArray</code>.
   * <p>This implementation looks only for constants of <code>entityClazz</code> type.</p>
   * @param clazz Class to parse.
   * @param entityClazz Type of constants to look for.
   * @param vectorOrArray Array o vector instance to push parsed constant values.
   * @return Filled <code>vectorOrArray</code> instance.
   * @throws ArgumentError If one of arguments is <code>null</code> or <code>vectorOrArray</code> neither Vector nor
   * Array, or <code>entityClazz</code> doesn't match <code>vectorOrArray</code> class.
   */
  public static function getClassTypedConstants( clazz:Class, entityClazz:Class, vectorOrArray:Object ):Object
  {
    if( !clazz || !entityClazz || !vectorOrArray )
    {
      throw new ArgumentError( "clazz, entityClazz and vectorOrArray must be non-null values" );
    }
    return getClassConstantsInternal( clazz, entityClazz, vectorOrArray );
  }

  /**
   * Parse <code>clazz</code> for all <b><code>public static const</code></b> values and push all of them
   * into <code>vectorOrArray</code>.
   * @param clazz Class to parse.
   * @param vectorOrArray Array o vector instance to push parsed constant values.
   * @return Filled <code>vectorOrArray</code> instance.
   * @throws ArgumentError If one of arguments is <code>null</code> or <code>vectorOrArray</code> neither Vector nor
   * Array.
   */
  public static function getClassConstants( clazz:Class, vectorOrArray:Object ):Object
  {
    if( !clazz || !vectorOrArray )
    {
      throw new ArgumentError( "clazz and vectorOrArray must be non-null values" );
    }
    return getClassConstantsInternal( clazz, null, vectorOrArray );
  }

  /**
   * Parse <code>clazz</code> for all <b><code>public static const</code></b> values and return <code>Array</code>
   * of those constants.
   * @param clazz Class to parse.
   * @return Non-null new <code>Array</code> of the class constant values.
   * @throws ArgumentError If <code>clazz</code> is <code>null</code>.
   */
  public static function getClassConstantsArray( clazz:Class ):Array
  {
    if( !clazz )
    {
      throw new ArgumentError( "clazz must be non-null values" );
    }
    return getClassConstantsInternal( clazz, null, [] ) as Array;
  }

  private static function getClassConstantsInternal( clazz:Class, entityClazz:Class, vectorOrArray:Object ):Object
  {
    validateVectorOrArray( vectorOrArray, entityClazz );

    var xml:XML = DescribeTypeCache.describeType( clazz ).typeDescription;
    var constants:XMLList;
    if( entityClazz )
    {
      constants = xml.constant.( @type == DescribeTypeCache.describeType( entityClazz ).typeDescription.@name );
    }
    else
    {
      constants = xml.constant;
    }

    vectorOrArray.length = constants.length();

    for( var i:int = 0; i < constants.length(); i++ )
    {
      xml = constants[i];
      vectorOrArray[i] = clazz[xml.@name];
    }

    return vectorOrArray;
  }

  /**
   * Checks the <code>instance</code> is either Array or Vector. If <code>entityClazz</code> is not null and instance is
   * a vector - validate vector type matches <code>entityClazz</code> type.
   * @param vectorOrArray
   * @param entityClazz If <code>null</code> - vector type would be not validated.
   * @throws ArgumentError If <code>vectorOrArray</code> is neither Vector nor Array, or <code>entityClazz</code>
   * doesn't match <code>vectorOrArray</code> class.
   */
  private static function validateVectorOrArray( vectorOrArray:Object, entityClazz:Class ):void
  {
    var sourceClassName:String = getQualifiedClassName( vectorOrArray );
    if( sourceClassName == ARRAY_NAME )
    {
      return;//this is an array
    }
    else if( sourceClassName.indexOf( VECTOR_PREFIX ) == 0 )
    {
      var entVec:String = VECTOR_PREFIX + "<" + getQualifiedClassName( entityClazz ) + ">";
      if( entityClazz && sourceClassName != entVec )
      {
        throw new ArgumentError( "The argument vectorOrArray is a Vector of wrong type: " + sourceClassName + " when " +
        "argument entityClazz expects " + entVec );
      }
      else
      {
        return;//this is a vector no matter what type
      }
    }

    throw new ArgumentError( "The argument vectorOrArray is neither vector nor array" );
  }

  private static const VECTOR_PREFIX:String = "__AS3__.vec::Vector.";
  private static const ARRAY_NAME:String = "Array";
}
}
