/**
 * @author: radistao
 * Date: 20.08.14 13:24
 */
package util {
    import flash.utils.ByteArray;

    public class StringConvertUtil {

        /**
         * Cut string to limit it byte newByteSize in UTF-8 representation.
         * <p>This function returns valid string, which does not have invalid UTF-8 characters and
         * the result may has byte length less, than <code>newByteSize</code>, if cutting occurs on
         * the middle of a character. So, the function guarantees the results string byte length
         * <i>not greater</i> than <code>newByteSize</code>.</p>
         * @param str String to cut. If <code>null</code> - the result will be <code>null</code>.
         * @param newByteSize Length in bytes to cut UTF-8 representation of the string. If greater
         * than string byte-length - uncut string will be returned
         * @return String, which has UTF-8 byte-length not greater than <code>newByteSize</code>.
         * <ul>
         *     <li>If input <code>str</code> is <code>null</code> - returns <code>null</code></li>
         *     <li>If input <code>str</code> is <i>empty</code> - returns empty string</li>
         *     <li>If input <code>str</code> byte-length is less than <code>newByteSize</code> -
         *     returns uncut original string.</li>
         * </ul>
         */
        public static function cutToUtfBytesLength(str:String, newByteSize:int):String {
            if (newByteSize < 0) {
                throw new ArgumentError("newByteSize argument must be not less than 0");
            }

            if (str == null) {
                return null;
            }

            if (newByteSize == 0) {
                return "";
            }

            var ba:ByteArray = new ByteArray();
            ba.writeUTFBytes(str);

            if (ba.length <= newByteSize) {
                return str;
            }

            ba.position = newByteSize - 1;
            var byteLast:int = ba.readByte();//signed value [-128; 127]

            if (isSingleByteCharacter(byteLast)) {
                //byteLast is valid character
                //and it is valid cut position
            } else {
                var backPositions:int = 1;//number of bytes from start byte to byteLast
                while (isMiddleByte(byteLast)) {
                    ba.position -= 2;
                    byteLast = ba.readByte();
                    backPositions++;
                }

                //now byteLast has information about how many bytes in the character
                var bytesInCharacter:int = 1;
                var marker:int = 0x40;
                while (marker && (marker & byteLast)) {
                    marker >>= 1;
                    bytesInCharacter++;
                }

                if (bytesInCharacter > backPositions) {
                    newByteSize -= backPositions;
                }
            }

            ba.position = 0;
            return ba.readUTFBytes(newByteSize);
        }

        /**
         * Same, as <code>cutToUtfBytesLength</code>, but other implementation, based on allocation
         * multi-byte first byte and cutting to previous one.
         * @see cutToUtfBytesLength
         */
        public static function cutToUtfBytesLength2(str:String, newLength:uint):String {
            if (str == null) {
                return null;
            }

            if (newLength == 0) {
                return "";
            }

            var ba:ByteArray = new ByteArray();
            ba.writeUTFBytes(str);

            if (ba.length <= newLength) {
                return str;
            }

            ba.position = newLength - 1;
            var byteLast:int = ba.readByte();//signed value [-128; 127]

            if (isSingleByteCharacter(byteLast)) {
                //byteLast is valid character
                //and it is valid cut position
            } else {
                if (isMultiByteFirstCharacter(byteLast)) {
                    //if it is first byte of multi-byte character
                    //just move one position back to last byte of uncut character
                    newLength--;
                } else {
                    var byte2:int = ba.readByte();//read next byte of the row
                    if (isMiddleByte(byte2)) {
                        //byteLast is not the last byte of multi-byte character
                        //go back until end of this character
                        do {
                            ba.position -= 2;
                            byteLast = ba.readByte();
                            newLength--;
                        } while (isMiddleByte(byteLast));
                    } //else {
                    //byteLast is the last byte of multi-bite character,
                    //so current cut position is valid
                    //}
                }
            }

            ba.position = 0;
            return ba.readUTFBytes(newLength);
        }

        /**
         * One byte character from ASCII table
         */
        private static function isSingleByteCharacter(byte:int):Boolean {
            return (0xFF & byte) < 0x80;
        }

        /**
         * First byte of multi-byte character
         */
        private static function isMultiByteFirstCharacter(byte:int):Boolean {
            return (0xC0 & byte) == 0xC0;
        }

        /**
         * Not a first byte of multi-byte character.
         */
        private static function isMiddleByte(byte:int):Boolean {
            return (0xC0 & byte) == 0x80;
        }
        
        /**
         * @return length in bytes of UTF-8 representation of argument string
         */
        public static function utfByteLength(s:String):int {
            var ba:ByteArray = new ByteArray();
            ba.writeUTFBytes(s);
            return ba.length;
        }
    }
}
