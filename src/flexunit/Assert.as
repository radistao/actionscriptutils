/**
 * @author akovalenko
 * @date 16.01.2015
 */
package flexunit {
import org.flexunit.Assert;

/**
 * @see  org.flexunit.Assert
 */
public class Assert
{
  /**
   * Asserts that a condition is strictly NaN.
   *
   * @param rest
   *      Accepts an argument of any type.
   *      If two arguments are passed the first argument must be a String
   *      and will be used as the error message.
   *
   *      <code>assertNaN( String, * );</code>
   *      <code>assertNaN( * );</code>
   */
  public static function assertNaN( ...rest ):void
  {
    org.flexunit.Assert._assertCount++;
    if( rest.length == 2 )
    {
      failNotNaN( rest[0], rest[1] );
    }
    else
    {
      failNotNaN( "", rest[0] );
    }
  }


  public static function failNotNaN( userMessage:String, value:* ):void
  {
    if( !(value is Number && isNaN( Number( value ) )) )
    {
      if( userMessage )
      {
        userMessage = userMessage + " - ";
      }
      org.flexunit.Assert.fail( userMessage + "expected <NaN> but was <" + String( value ) + ">" );
    }
  }
}
}