/**
 * @author akovalenko
 * @date 16.01.2015
 */
package flexunit.asserts
{
import flexunit.Assert;

public function assertNaN( ...rest ):void
{
  /**
   * Alias flexunit.Assert assertNaN method
   *
   * @param rest
   *      Accepts an argument of any type.
   *      If two arguments are passed the first argument must be a String
   *      and will be used as the error message.
   *
   *      <code>assertNaN( String, * );</code>
   *      <code>assertNaN( * );</code>
   *
   * @see flexunit.Assert assertNaN
   */

  Assert.assertNaN.apply( null, rest );
}
}
