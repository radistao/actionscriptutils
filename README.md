# ActionScript Utils #

This repository is to store and free distribute some common AS3 solutions, which are not found in standard libraries, like *Flex SDK*, *as3corelib*

### Implemented features ###

* **`StringConvertUtil`**:
    * *cutToUtfBytesLength* function to limit ActionScript `String` (which is UTF-16) to some UTF-8 bytes length.
    * *utfByteLength* function to calculate length in bytes of UTF-8 representation of input string. 
    
* **`EnumUtil`**:
    * These util is intended to use some classes as constant enums and list all static constants of a class. It uses reflection (`describeType`) to parse class consts.
    * *getClassTypedConstants* Parse some class for **`public static const`** values of certain type
    * *getClassConstants* Parse some class for all **`public static const`** values and push them to array
    * *getClassConstantsArray* Parse some class for all **`public static const`** values and return new Array

* **`flexunit`**:
    * *assertNaN* Asserts that a condition is strictly NaN (not zero, null, undefined etc)

### How to use ###

* Clone or download the repository
* or just copy-paste the functions.

### License ###
[MIT](http://opensource.org/licenses/MIT) (I guess this is the simplest one)

### Bugs and issues###
If you discovered any bugs\issues, pls use the [Issue tracker](https://bitbucket.org/radistao/actionscriptutils/issues) or contact me directly.