/**
 * @author akovalenko
 * @date 16.01.2015
 */
package asserts
{
import flexunit.asserts.*;

/**
 * Test assertNaN fails on all "null-like" values
 */
public class AssertNotNaNCases
{

  [Test]
  public function test_assertNaN():void
  {
    assertNaN( "assertNaN fault", NaN );
    assertNaN( "assertNaN fault", Number.NaN );
    assertNaN( "assertNaN fault", 0 / 0 );

    var t:Object = {nan: NaN};
    assertNaN( "assertNaN fault", t.nan );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_intZero():void
  {
    assertNaN( "Zero test fault", 0 );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_NumberZero():void
  {
    assertNaN( "Zero test fault", 0.0 );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_Infinity():void
  {
    assertNaN( "Infinity test fault", Number.POSITIVE_INFINITY );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_InfinityNegative():void
  {
    assertNaN( "Infinity test fault", Number.NEGATIVE_INFINITY );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_null():void
  {
    assertNaN( "Null test fault", null );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_emptyString():void
  {
    assertNaN( "Empty String test fault", "" );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_false():void
  {
    assertNaN( "False test fault", false );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_undefined():void
  {
    assertNaN( "undefined test fault", undefined );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_undefined2():void
  {
    assertNaN( "undefined2 test fault", {}.foo );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_numeric():void
  {
    assertNaN( "numeric test fault", 5.7 );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_string():void
  {
    assertNaN( "String test fault", "foo" );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_boolean():void
  {
    assertNaN( "Boolean test fault", false );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_object():void
  {
    assertNaN( "Object test fault", {} );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function test_array():void
  {
    assertNaN( "Array test fault", [] );
  }
}
}
