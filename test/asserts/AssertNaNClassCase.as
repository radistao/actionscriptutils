/**
 * @author akovalenko
 * @date 16.01.2015
 */
package asserts
{
import flexunit.Assert;
import flexunit.framework.AssertionFailedError;

import org.flexunit.Assert;

public class AssertNaNClassCase
{
  [Test(description="Ensure that the assertNaN function correctly works when a NaN value is provided")]
  public function shouldPassWhenNaN():void
  {
    flexunit.Assert.assertNaN( NaN );
  }

  [Test(description="Ensure that the assertNaN function correctly works when a NaN value and a message are provided")]
  public function shouldPassWhenNaNWithCustomMessage():void
  {
    flexunit.Assert.assertNaN( "Assert NaN fail", NaN );
  }

  [Test(description="Ensure that the assertNaN function fails when other value is provided")]
  public function shouldFailWhenValue():void
  {
    var failed:Boolean = false;
    var message:String;
    var value:Number = 1;
    try
    {
      flexunit.Assert.assertNaN( value )
    }
    catch( error:AssertionFailedError )
    {
      failed = true;
      message = error.message;
    }

    if( !failed )
    {
      org.flexunit.Assert.fail( "Assert NaN didn't fail" );
    }

    org.flexunit.Assert.assertEquals( "expected <NaN> but was <1>", message );
  }

  [Test(description="Ensure that the assertNaN functions fails when a false value is provided and the proper passed message is displayed")]
  public function shouldFailWhenValueWithCustomMessage():void
  {
    var message:String;
    var failed:Boolean = false;
    var value:Number = 0;

    try
    {
      flexunit.Assert.assertNaN( "Yo Yo Yo", value )
      // if we get an error with the right message we pass
    }
    catch( error:AssertionFailedError )
    {
      failed = true;
      message = error.message;
    }

    if( !failed )
    {
      org.flexunit.Assert.fail( "Assert true didn't fail" );
    }

    org.flexunit.Assert.assertEquals( "Yo Yo Yo - expected <NaN> but was <0>", message );
  }
}
}