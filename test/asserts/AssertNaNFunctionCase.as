/**
 * @author akovalenko
 * @date 16.01.2015
 */
package asserts
{
import flexunit.asserts.*;
import flexunit.framework.AssertionFailedError;

import org.flexunit.Assert;

public class AssertNaNFunctionCase
{
  [Test(description="Ensure that the assertNaN function correctly works when a NaN value is provided")]
  public function shouldPassWhenNaN():void
  {
    assertNaN( NaN );
  }

  [Test(description="Ensure that the assertNaN function correctly works when NaN value and a message are provided")]
  public function shouldPassWhenTrueWithCustomMessage():void
  {
    assertNaN( "Assert NaN fail", NaN );
  }

  [Test(description="Ensure that the assertNaN function fails when a other value is provided")]
  public function shouldFailWhenValue():void
  {
    var failed:Boolean = false;
    var message:String;
    var value:Boolean = false;

    try
    {
      assertNaN( value )
    }
    catch( error:AssertionFailedError )
    {
      failed = true;
      message = error.message;
    }

    if( !failed )
    {
      Assert.fail( "Assert NaN didn't fail" );
    }

    Assert.assertEquals( "expected <NaN> but was <false>", message );
  }

  [Test(description="Ensure that the assertNaN functions fails when a true value is provided and the proper passed message is displayed")]
  public function shouldFailWhenValueWithCustomMessage():void
  {
    var message:String;
    var failed:Boolean = false;

    try
    {
      assertNaN( "Yo Yo Yo", true )
      // if we get an error with the right message we pass
    }
    catch( error:AssertionFailedError )
    {
      failed = true;
      message = error.message;
    }

    if( !failed )
    {
      Assert.fail( "Assert true didn't fail" );
    }

    Assert.assertEquals( "Yo Yo Yo - expected <NaN> but was <true>", message );
  }
}
}