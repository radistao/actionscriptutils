/**
 * @author akovalenko
 * @date 16.01.2015
 */
package asserts
{
import flexunit.Assert;
import flexunit.framework.AssertionFailedError;

import org.flexunit.Assert;

public class AssertFailNotNaN
{
  [Test]
  public function shouldPassNaN():void
  {
    flexunit.Assert.failNotNaN( "Fail not NaN fail", NaN );
  }

  [Test(expects="flexunit.framework.AssertionFailedError")]
  public function shouldFailNaN():void
  {
    flexunit.Assert.failNotNaN( "Fail not NaN fail", 0.0 );
  }

  [Test(description="Ensure that the failNotNaN function fails when a value of significant is provided")]
  public function shouldFailWithCustomMessage():void
  {
    var failed:Boolean = false;
    var message:String;

    try
    {
      flexunit.Assert.failNotNaN( "Yo Yo Yo", 0 );
    }
    catch( error:AssertionFailedError )
    {
      failed = true;
      message = error.message;
    }

    if( !failed )
    {
      org.flexunit.Assert.fail( "Fail not NaN fail didn't fail" );
    }

    org.flexunit.Assert.assertEquals( "Yo Yo Yo - expected <NaN> but was <0>", message );
  }

}
}