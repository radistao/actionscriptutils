/**
 * @author akovalenko
 * @date 19.02.2015
 */
package enumUtil
{
public class TestEnumStrings
{
  public static const STRING_CONSTANT1:String = "String Constant 1";
  public static const STRING_CONSTANT2:String = "String Constant 2";
  public static const STRING_CONSTANT3:String = "String Constant 3";
  public static const STRING_CONSTANT4:String = "String Constant 4";
  public static const STRING_CONSTANT5:String = "String Constant 5";
}
}
