/**
 * @author akovalenko
 * @date 20.02.2015
 */
package enumUtil
{
public class TestEnumMixedConstants
{
  public static const STRING_CONSTANT1:String = "String Constant 1";
  public static const STRING_CONSTANT2:String = "String Constant 2";
  public static const STRING_CONSTANT3:String = "String Constant 3";
  public static const STRING_CONSTANT4:String = "String Constant 4";
  public static const STRING_CONSTANT5:String = "String Constant 5";

  public static const CLASS_CONSTANT1:TestVO = new TestVO( 1, "Name 1" );
  public static const CLASS_CONSTANT2:TestVO = new TestVO( 2, "Name 2" );
  public static const CLASS_CONSTANT3:TestVO = new TestVO( 3, "Name 3" );
  public static const CLASS_CONSTANT4:TestVO = new TestVO( 4, "Name 4" );

  public static const INT_CONSTANT1:int = 1;
  public static const INT_CONSTANT2:int = 2;
  public static const INT_CONSTANT3:int = 3;
}
}
