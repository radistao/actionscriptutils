/**
 * @author akovalenko
 * @date 19.02.2015
 */
package enumUtil
{
public class TestVO
{
  public static var someStaticVar:int = 10;

  public const someConst:String = "Some Const";

  public var id:int;

  public var name:String;

  public function TestVO( id:int, name:String )
  {
    this.id = id;
    this.name = name;
  }

  public function toString():String
  {
    return id + ": " + name;
  }
}
}
