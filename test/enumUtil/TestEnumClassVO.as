/**
 * @author akovalenko
 * @date 19.02.2015
 */
package enumUtil
{
public class TestEnumClassVO
{
  public static const CLASS_CONSTANT1:TestVO = new TestVO( 1, "Name 1" );
  public static const CLASS_CONSTANT2:TestVO = new TestVO( 2, "Name 2" );
  public static const CLASS_CONSTANT3:TestVO = new TestVO( 3, "Name 3" );
  public static const CLASS_CONSTANT4:TestVO = new TestVO( 4, "Name 4" );
}
}
