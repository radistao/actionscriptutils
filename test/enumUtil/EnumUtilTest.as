/**
 * @author akovalenko
 * @date 19.02.2015
 */
package enumUtil
{
import org.flexunit.asserts.assertEquals;
import org.flexunit.asserts.assertNotNull;
import org.flexunit.asserts.assertTrue;

import util.EnumUtil;

public class EnumUtilTest
{

  ////////////////////////////////////////////////////////////
  //Test argument exceptions
  ////////////////////////////////////////////////////////////

  [Test(description="Test exception of class argument", expects="ArgumentError")]
  public function test_getClassConstantsErrorClass():void
  {
    EnumUtil.getClassConstants( null, [] );
  }

  [Test(description="Test exception of array argument if null", expects="ArgumentError")]
  public function test_getClassConstantsErrorArray1():void
  {
    EnumUtil.getClassConstants( TestEnumStrings, null );
  }

  [Test(description="Test exception of array argument if not a list", expects="ArgumentError")]
  public function test_getClassConstantsErrorArray2():void
  {
    EnumUtil.getClassConstants( TestEnumStrings, {} );
  }

  [Test(description="Test exception of class argument", expects="ArgumentError")]
  public function test_getClassTypedConstantsErrorClass():void
  {
    EnumUtil.getClassTypedConstants( null, TestVO, [] );
  }

  [Test(description="Test exception of entity type argument if null", expects="ArgumentError")]
  public function test_getClassTypedConstantsErrorEntity():void
  {
    EnumUtil.getClassTypedConstants( TestEnumClassVO, null, [] );
  }

  [Test(description="Test exception of entity type argument if null", expects="ArgumentError")]
  public function test_getClassTypedConstantsErrorArray1():void
  {
    EnumUtil.getClassTypedConstants( TestEnumClassVO, TestVO, null );
  }

  [Test(description="Test exception of vectorOrArray argument if neither vector nor array", expects="ArgumentError")]
  public function test_getClassTypedConstantsErrorArray2():void
  {
    EnumUtil.getClassTypedConstants( TestEnumClassVO, TestVO, {} );
  }

  [Test(description="Test exception of vectorOrArray argument if vector of wrong type", expects="ArgumentError")]
  public function test_getClassTypedConstantsErrorArray3():void
  {
    EnumUtil.getClassTypedConstants( TestEnumClassVO, TestVO, new Vector.<String> );
  }

  ////////////////////////////////////////////////////////////
  //Test getClassConstants and getClassTypedConstants methods
  ////////////////////////////////////////////////////////////

  [Test(description="Test getClassConstants as untyped enumer")]
  public function test_getClassConstants():void
  {
    //test class constants as array
    var constantsArray:Array = EnumUtil.getClassConstants( TestEnumClassVO, [] ) as Array;

    assertEquals( 4, constantsArray.length );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT1 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT2 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT3 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT4 ) > -1 );

    //test string constants as vector
    var constantsVector:Vector.<String> = EnumUtil.getClassConstants( TestEnumStrings, new Vector.<String>() ) as Vector.<String>;

    assertEquals( 5, constantsVector.length );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT1 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT2 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT3 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT4 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT5 ) > -1 );

    //test mixed constants as array
    constantsArray = EnumUtil.getClassConstants( TestEnumMixedConstants, [] ) as Array;

    assertEquals( 12, constantsArray.length );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.CLASS_CONSTANT1 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.INT_CONSTANT2 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.STRING_CONSTANT3 ) > -1 );

    //test mixed constants as vector
    var constantsVector2:Vector.<Object> = EnumUtil.getClassConstants( TestEnumMixedConstants, new Vector.<Object>() ) as Vector.<Object>;

    assertEquals( 12, constantsVector2.length );
    assertTrue( constantsVector2.indexOf( TestEnumMixedConstants.CLASS_CONSTANT3 ) > -1 );
    assertTrue( constantsVector2.indexOf( TestEnumMixedConstants.INT_CONSTANT1 ) > -1 );
    assertTrue( constantsVector2.indexOf( TestEnumMixedConstants.STRING_CONSTANT4 ) > -1 );

    //test class without static constants, but with constant members and static properties
    assertEquals( "Just to ensure TestVO has public variables", "20", (new TestVO( 50, "20" )).name );
    assertEquals( "Just to ensure TestVO has public const", "Some Const", (new TestVO( 50, "20" )).someConst );
    assertEquals( "Just to ensure TestVO has public static variables", 10, TestVO.someStaticVar );

    constantsArray = EnumUtil.getClassConstants( TestVO, [] ) as Array;
    assertNotNull( constantsArray );
    assertEquals( 0, constantsArray.length );

    constantsVector2 = EnumUtil.getClassConstants( TestVO, new Vector.<Object>() ) as Vector.<Object>;
    assertNotNull( constantsVector2 );
    assertEquals( 0, constantsVector2.length );
  }

  [Test(description="Test getClassConstants as untyped enumer")]
  public function test_getClassTypedConstants():void
  {
    var constantsArray:Array = EnumUtil.getClassTypedConstants( TestEnumClassVO, TestVO, [] ) as Array;

    assertEquals( 4, constantsArray.length );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT1 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT2 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT3 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumClassVO.CLASS_CONSTANT4 ) > -1 );

    //elements of other type should be empty
    constantsArray = EnumUtil.getClassTypedConstants( TestEnumClassVO, String, [] ) as Array;
    assertNotNull( constantsArray );
    assertEquals( 0, constantsArray.length );

    var constantsVector:Vector.<String> = EnumUtil.getClassTypedConstants( TestEnumStrings, String, new Vector.<String>() ) as Vector.<String>;

    assertEquals( 5, constantsVector.length );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT1 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT2 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT3 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT4 ) > -1 );
    assertTrue( constantsVector.indexOf( TestEnumStrings.STRING_CONSTANT5 ) > -1 );

    //no TestVO constants in the
    var t:Vector.<TestVO> = EnumUtil.getClassTypedConstants( TestEnumStrings, TestVO, new Vector.<TestVO>() ) as Vector.<TestVO>;
    assertNotNull( t );
    assertEquals( 0, t.length );

    //ONLY TestVO constants
    constantsArray = EnumUtil.getClassTypedConstants( TestEnumMixedConstants, TestVO, [] ) as Array;
    assertEquals( 4, constantsArray.length );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.CLASS_CONSTANT1 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.CLASS_CONSTANT2 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.CLASS_CONSTANT3 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.CLASS_CONSTANT4 ) > -1 );

    //ONLY String constants
    constantsArray = EnumUtil.getClassTypedConstants( TestEnumMixedConstants, String, [] ) as Array;
    assertEquals( 5, constantsArray.length );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.STRING_CONSTANT1 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.STRING_CONSTANT2 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.STRING_CONSTANT3 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.STRING_CONSTANT4 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.STRING_CONSTANT5 ) > -1 );

    //ONLY int constants
    constantsArray = EnumUtil.getClassTypedConstants( TestEnumMixedConstants, int, [] ) as Array;
    assertEquals( 3, constantsArray.length );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.INT_CONSTANT1 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.INT_CONSTANT2 ) > -1 );
    assertTrue( constantsArray.indexOf( TestEnumMixedConstants.INT_CONSTANT3 ) > -1 );

    //repeat the same with vectors

    var constantsVectorTestVO:Vector.<TestVO> = EnumUtil.getClassTypedConstants( TestEnumMixedConstants, TestVO, new Vector.<TestVO>() ) as Vector.<TestVO>;
    assertEquals( 4, constantsVectorTestVO.length );
    assertTrue( constantsVectorTestVO.indexOf( TestEnumMixedConstants.CLASS_CONSTANT1 ) > -1 );
    assertTrue( constantsVectorTestVO.indexOf( TestEnumMixedConstants.CLASS_CONSTANT2 ) > -1 );
    assertTrue( constantsVectorTestVO.indexOf( TestEnumMixedConstants.CLASS_CONSTANT3 ) > -1 );
    assertTrue( constantsVectorTestVO.indexOf( TestEnumMixedConstants.CLASS_CONSTANT4 ) > -1 );

    var constantsVectorString:Vector.<String> = EnumUtil.getClassTypedConstants( TestEnumMixedConstants, String, new Vector.<String>() ) as Vector.<String>;
    assertEquals( 5, constantsVectorString.length );
    assertTrue( constantsVectorString.indexOf( TestEnumMixedConstants.STRING_CONSTANT1 ) > -1 );
    assertTrue( constantsVectorString.indexOf( TestEnumMixedConstants.STRING_CONSTANT2 ) > -1 );
    assertTrue( constantsVectorString.indexOf( TestEnumMixedConstants.STRING_CONSTANT3 ) > -1 );
    assertTrue( constantsVectorString.indexOf( TestEnumMixedConstants.STRING_CONSTANT4 ) > -1 );
    assertTrue( constantsVectorString.indexOf( TestEnumMixedConstants.STRING_CONSTANT5 ) > -1 );

    var constantsVectorInt:Vector.<int> = EnumUtil.getClassTypedConstants( TestEnumMixedConstants, int, new Vector.<int>() ) as Vector.<int>;
    assertEquals( 3, constantsVectorInt.length );
    assertTrue( constantsVectorInt.indexOf( TestEnumMixedConstants.INT_CONSTANT1 ) > -1 );
    assertTrue( constantsVectorInt.indexOf( TestEnumMixedConstants.INT_CONSTANT2 ) > -1 );
    assertTrue( constantsVectorInt.indexOf( TestEnumMixedConstants.INT_CONSTANT3 ) > -1 );

    //test class without static constants, but with constant members and static properties
    constantsArray = EnumUtil.getClassTypedConstants( TestVO, int, [] ) as Array;
    assertNotNull( constantsArray );
    assertEquals( 0, constantsArray.length );

    var tStr:Vector.<String> = EnumUtil.getClassTypedConstants( TestVO, String, new Vector.<String>() ) as Vector.<String>;
    assertNotNull( tStr );
    assertEquals( 0, tStr.length );

    var tInt:Vector.<int> = EnumUtil.getClassTypedConstants( TestVO, int, new Vector.<int>() ) as Vector.<int>;
    assertNotNull( tInt );
    assertEquals( 0, tInt.length );
  }
}
}
